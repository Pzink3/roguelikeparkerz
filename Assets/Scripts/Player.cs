﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Player : MovingObject {

    public Text healthText;
    public AudioClip movementSound1;
    public AudioClip movementSound2;
    public AudioClip chopSound1;
    public AudioClip chopSound2;
    public AudioClip fruitSound1;
    public AudioClip fruitSound2;
    public AudioClip sodaSound1;
    public AudioClip sodaSound2;
    public AudioClip exitSound;
    public AudioClip coinSound;
    public AudioClip ticketSound;
    public AudioClip tokenSound;
    public AudioClip r2D2Sound;
    public AudioClip bB8Sound;
    public AudioClip marioCoinSound;
 
    
    private Animator animator;

    private int playerHealth;
    private int attackPower = 1;
    private int healthPerFruit = 5;
    private int healthPerSoda = 10;
    private int healthPerCoin = 20;
    private int healthPerBB8Token = 8;
    private int healthPerBB8 = 8;
    private int healthPerMarioCoin = 5;
    private int healthPerStarCoin = 15;
    private int healthPerCoin50 = 5;
    private int healthPerR2D2 = 2;
    private int healthPerTicket = 7;
    private int secondsUntilNewLevel = 1;

    private int minScreenWidth;
    private int maxScreenWidth;
    private int tileSize;
    private int xAxis = 0;
    private int yAxis = 0;


    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        playerHealth = GameController.Instance.playerCurrentHealth;
        healthText.text = "Lives: " + playerHealth;
    }

    private void OnDisable()
    {
        GameController.Instance.playerCurrentHealth = playerHealth;
    }

    void Update () {
       
        if(!GameController.Instance.isPlayerTurn)
        {
            return;
        }
        CheckIfGameOver();
        int xAxis = 0;
        int yAxis = 0;

        if (Input.touchCount > 0)
        {
            Vector2 touchPosition = Input.GetTouch(0).position;
            xAxis = (int)touchPosition.x;
            yAxis = (int)touchPosition.y;

            GetScreenSize();


            if(xAxis >= minScreenWidth && xAxis <= maxScreenWidth)
            {
                ExecuteTouch();
            } else
            {
                xAxis = 0;
                yAxis = 0;
            }
        }
        else
        {

            xAxis = (int)Input.GetAxisRaw("Horizontal");
            yAxis = (int)Input.GetAxisRaw("Vertical");

            if (xAxis != 0)
            {
                yAxis = 0;
            }
        }

        if(xAxis != 0 || yAxis != 0)
        {
            playerHealth--;
            healthText.text = "Lives: " + playerHealth;
            SoundController.Instance.PlaySingle(movementSound1, movementSound2);
            Move<Wall>(xAxis, yAxis);
            GameController.Instance.isPlayerTurn = false;
        }

       
	}


    private void GetScreenSize()
    {
        int screenWidth = Screen.currentResolution.width;
        int screenHeight = Screen.currentResolution.height;
        minScreenWidth = (screenWidth - screenHeight) / 2;
        maxScreenWidth = minScreenWidth + screenHeight;
        tileSize = screenHeight / 10;
    }

    private void ExecuteTouch()
    {
        Vector2 currentPosition = this.getPosition();

        CorrectForScreens();

        if(Mathf.Abs(xAxis - (int)currentPosition.x) >
           Mathf.Abs (yAxis - (int)currentPosition.y))
        {
            MoveHorizontal(currentPosition);
        } else
        {
            MoveVertical(currentPosition);
        }
    }

    private void CorrectForScreens()
    {
        xAxis -= minScreenWidth;
        xAxis /= tileSize;
        yAxis /= tileSize;
    }

    private void MoveHorizontal(Vector2 currentPosition)
    {
        yAxis = 0;
        if (xAxis - (int)currentPosition.x > 0)
        {
            xAxis = 1;
        } else if(xAxis - (int)currentPosition.x < 0)
        {
            xAxis = -1;
        }
        else
        {
            xAxis = 0;
        }
    }


    private void MoveVertical(Vector2 currentPosition)
    {
        xAxis = 0;
        if (yAxis - (int)currentPosition.y > 0)
        {
            yAxis = 1;
        } else if (yAxis - (int)currentPosition.y < 0)
        {
            yAxis = -1;
        } else
        {
            yAxis = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if (objectPlayerCollidedWith.tag == "Exit")
        {
            Invoke("LoadNewLevel", secondsUntilNewLevel);
            enabled = false;
            SoundController.Instance.PlaySingle(exitSound);
            healthText.text = "<color=lime>Area Clear!</color>";
        }
        else if(objectPlayerCollidedWith.tag == "Fruit")
        {
            playerHealth += healthPerFruit;
            healthText.text = "+" + healthPerFruit + " Lives";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
        }
        else if(objectPlayerCollidedWith.tag == "Soda")
        {
            playerHealth += healthPerSoda;
            healthText.text = "+" + healthPerSoda + " Lives";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
        }
        else if (objectPlayerCollidedWith.tag == "Coin")
        {
            playerHealth += healthPerCoin;
            healthText.text = "<color=yellow>+</color>" + healthPerCoin + " <color=yellow>Lives</color>";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(coinSound);
        }
        else if (objectPlayerCollidedWith.tag == "StarCoin")
        {
            playerHealth += healthPerStarCoin;
            healthText.text = "<color=yellow>+</color>" + healthPerStarCoin + " <color=yellow>Lives</color>";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(coinSound);
        }
        else if (objectPlayerCollidedWith.tag == "Coin50")
        {
            playerHealth += healthPerCoin50;
            healthText.text = "<color=yellow>+</color>" + healthPerCoin50 + " <color=yellow>Lives</color>";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(coinSound);
        }
        else if (objectPlayerCollidedWith.tag == "Ticket")
        {
            playerHealth += healthPerTicket;
            healthText.text = "+" + healthPerTicket + " bonus!";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(ticketSound);
        }
        else if (objectPlayerCollidedWith.tag == "Token")
        {
            playerHealth += healthPerBB8Token;
            healthText.text = "<color=orange>BB-</color>" + healthPerBB8Token + " bonus!";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(tokenSound);
        }
        else if (objectPlayerCollidedWith.tag == "R2D2")
        {
            playerHealth += healthPerR2D2;
            healthText.text = "<color=blue>R</color>" + healthPerR2D2 + "-" + "<color=blue>D</color>" + healthPerR2D2 + " <color=blue>bonus!</color>";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(r2D2Sound);
        }
        else if (objectPlayerCollidedWith.tag == "BB8")
        {
            playerHealth += healthPerBB8;
            healthText.text = "<color=orange>BB-</color>" + healthPerBB8 + " bonus!";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(bB8Sound);
        }
        else if (objectPlayerCollidedWith.tag == "MarioCoin")
        {
            playerHealth += healthPerMarioCoin;
            healthText.text = "<color=yellow>Mario bonus!</color>";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(marioCoinSound);
        }
        else if (objectPlayerCollidedWith.tag == "GoldCoin")
        {
            playerHealth += healthPerCoin;
            healthText.text = "<color=yellow>Coin bonus!</color>";
            objectPlayerCollidedWith.gameObject.SetActive(false);
            SoundController.Instance.PlaySingle(coinSound);
        }
    }

    private void LoadNewLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }


    protected override void HandleCollision<T>(T component)
    {
        Wall wall = component as Wall;
        animator.SetTrigger("playerAttack");
        SoundController.Instance.PlaySingle(chopSound1, chopSound2);
        wall.DamageWall(attackPower);
    }

    public void TakeDamage(int damageReceived)
    {
        playerHealth -= damageReceived;
        healthText.text = "<color=red>-</color>" + damageReceived + " Lives";
        animator.SetTrigger("playerHurt");
    }

    private void CheckIfGameOver()
    {
        if(playerHealth <= 0)
        {
            GameController.Instance.GameOver();

        }
    }
}
